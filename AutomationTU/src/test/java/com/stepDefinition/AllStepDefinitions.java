package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllStepDefinitions {

	public static WebDriver driver;

	// ** Elements **//
	public static By COOKIE = By.cssSelector("#consent_prompt_submit");
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");
	public static By SEARCHTEXT = By.cssSelector(".tu-u-h1--secondary");
	public static By LOGINID = By.cssSelector("#j_username");
	public static By LOGINPASSWORD = By.cssSelector("#j_password");
	public static By SUBMITLOGIN = By.cssSelector("#submit-login");
	public static By RECAPTCHA = By.cssSelector(".ln-u-flush-bottom"); // ln-u-h4 ln-u-flush-bottom
	public static By WRONGMAILID = By.cssSelector(".ln-c-field-info--error");
	public static By LOGINREG = By.linkText("Tu Log In / Register");
	public static By SELECTDROP = By.cssSelector("#register_title");
	public static By REGISTER = By.cssSelector("[data-testid=registerButton]");
	public static By PROMOTIONS = By.cssSelector("input[data-testid='email-input']");
	public static By REGTU = By.cssSelector(".registerFormHolder h2");
	public static By REGISTEREMAIL = By.cssSelector("#register_email");
	public static By FIRSTNAME = By.cssSelector("#register_firstName");
	public static By LASTNAME = By.cssSelector("#register_lastName");
	public static By PASSWORD = By.cssSelector("#password");
	public static By CONFIRMPASSWORD = By.cssSelector("#register_checkPwd");
	public static By PROMOTIONSCLK = By.cssSelector("[data-testid=submit-button] svg");
	public static By PROMOTIONSTXT = By.cssSelector(".ln-u-display-flex");
	public static By HELPCENTRE = By.linkText("Help Centre");
	public static By TUHELP = By.linkText("Tu Help");
	public static By CONTACTUS = By.linkText("Contact us");
	public static By RETURNSNREFUNDS = By.linkText("Returns & refunds");
	public static By MYACCOUNT = By.linkText("My account");
	public static By COOKIEPOLICY = By.linkText("Cookie Policy – New");
	public static By OURCOOKIE = By.cssSelector(".section_title");
	public static By PRIVACYPOLICY = By.linkText("Privacy Policy – New");
	public static By SAINPRIVACY = By.cssSelector(".section-heading__text");
	public static By ARGOS = By.linkText("Argos");
	public static By ARGOSCOOKIE = By.cssSelector("[data-test=confirmation-button]");
	public static By ARGOSHOME = By.linkText("Argos Logo - Load homepage");
	public static By HABITATHOME = By.linkText("Habitat");
	public static By HABITATCOOKIE = By.cssSelector("#onetrust-accept-btn-handler");// u_0_2m
	public static By TERMS = By.linkText("Terms & conditions");
	public static By PRIVACYHUB = By.linkText("Privacy hub");
	public static By TERMSPAGE = By.cssSelector(".content h1");
	// public static By FB =
	// By.cssSelector("[xlink:href='/_ui/2017/symbol/svg/sprite.symbol.svg#Facebook']");
	public static By FB = By.xpath("//a[@href = 'https://www.facebook.com/tuclothing']");
	public static By TWITTER = By.xpath("//a[@href = 'https://www.twitter.com/tu_clothing']");
	public static By INSTAGRAM = By.xpath("//a[@href = 'https://www.instagram.com/tuclothing']");
	public static By INSTAGRAMPAGE = By.cssSelector(".nZSzR h1");
	public static By GPLUS = By.xpath("//a[@href = 'https://plus.google.com/111345982334725582630']");
	public static By PINTREST = By.xpath("//a[@href = 'https://www.pinterest.com/tuclothing']");
	public static By PINTRESTPAGE = By.cssSelector(".VxL h1");
	public static By GPLUSPAGE = By.cssSelector(".Y4dIwd");

	// public static By CHRISTMASTSHIRT = By.xpath("//a[@href =
	// '/c/christmas/women-christmas-t-shirts?INITD=GNav-ChristmasWW-ChristmasTshirts']");
	public static By DELIVERYINFORMATION = By.linkText("Delivery information");
	public static By DELIVERYINFORMATIONPAGE = By.cssSelector(".content h1");
	public static By CHRISTMASTSHIRT = By.cssSelector("[title=Christmas T-Shirts]");
	public static By CHRISTMAS = By.cssSelector("[title=Christmas]");
	public static By TWITTERTU = By.xpath("//a[@href = '/tu_clothing/photo']");
	public static By BLOUSES = By.cssSelector("#Blouses-d");
	public static By SHIRT = By.cssSelector("#Blouses-d");
	// a href="https://www.facebook.com/tuclothing"
	// <a href="/docs/configuration">App Configuration</a>
	// Driver.findElement(By.xpath(//a[@href ='/docs/configuration']")).click();

	public static By FBTU = By.cssSelector("._64-f");// fb_logo img sp_d7hJOvRHpT5 sx_90263d
	public static By SAINGPPRIVACY = By.cssSelector(".ln-c-card--soft h1");// ln-c-card home__wrapper ln-c-card--soft
	public static By FBCOOKIE = By.cssSelector("#u_0_2m");// u_0_2m
	// public static By TWITTERTU = By.cssSelector(".r-qvutc0");//css-901oao
	// css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0
	public static By TWITTERCOOKIE = By.cssSelector("#u_0_2m");// class="css-901oao css-16my406 r-1qd0xha r-ad9z0x
																// r-bcqeeo r-qvutc0"

	/*
	 * public static By RECAPTCHA =
	 * By.cssSelector(".ln-u-flush-bottom");title="Christmas T-Shirts" public static
	 * By FBTU = By.cssSelector("._64-f");
	 * 
	 * 
	 * 
	 * public static By WRONGMAILID = By.cssSelector(".ln-c-field-info--error");
	 */

	// ** Functionality starts here **//

	@Before
	public void start() {

		System.setProperty("webdriver.chrome.driver", "/Users/havanur/Desktop/Shruthi_Automation/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());

	}

	@Given("^I am in home page$")
	public void i_am_in_home_page() throws Throwable {
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion | Tu clothing", driver.getTitle());
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(COOKIE));
		driver.findElement(COOKIE).click();
	}

	@When("^I search valid data$")
	public void i_search_valid_data() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("Jeans");
		driver.findElement(SEARCHBUTTON).click();

	}

	@Then("^I should related search results page$")
	public void i_should_related_search_results_page() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(SEARCHTEXT));
		Assert.assertEquals("SEARCH RESULTS FOR", driver.findElement(SEARCHTEXT).getText());

	}

	@When("^I search invalid data$")
	public void i_search_invalid_data() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("asdf");
		driver.findElement(SEARCHBUTTON).click();

	}

	@Then("^I should related no results page$")
	public void i_should_related_no_results_page() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(SEARCHTEXT));
		Assert.assertEquals("SEARCH RESULTS FOR", driver.findElement(SEARCHTEXT).getText());

	}

	@When("^I login with valid data$")
	public void i_login_with_valid_data() throws Throwable {
		driver.findElement(LOGINREG).click();
		WebDriverWait wait1 = new WebDriverWait(driver, 20);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(LOGINID));
		driver.findElement(LOGINID).clear();
		driver.findElement(LOGINID).sendKeys("shruthi.a12@gmail.com");
		driver.findElement(LOGINPASSWORD).clear();
		driver.findElement(LOGINPASSWORD).sendKeys("Test@123");
		driver.findElement(SUBMITLOGIN).click();

	}

	@Then("^I should get captcha tick$")
	public void i_should_get_captcha_tick() throws Throwable {
		WebDriverWait wait1 = new WebDriverWait(driver, 20);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(SUBMITLOGIN));
		Assert.assertEquals("Please check the recaptcha", driver.findElement(RECAPTCHA).getText());

	}

	@When("^I login with invalid data$")
	public void i_login_with_invalid_data() throws Throwable {
		driver.findElement(LOGINREG).click();
		WebDriverWait wait1 = new WebDriverWait(driver, 20);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(LOGINID));
		driver.findElement(LOGINID).clear();
		driver.findElement(LOGINID).sendKeys("abc");
		// driver.findElement(LOGINPASSWORD).clear();
		// driver.findElement(LOGINPASSWORD).sendKeys("Test@123");
		driver.findElement(SUBMITLOGIN).click();
		wait1.until(ExpectedConditions.visibilityOfElementLocated(SUBMITLOGIN));

	}

	@Then("^I should get captcha tick option$")
	public void i_should_get_captcha_tick_option() throws Throwable {
		Assert.assertEquals("Please check the recaptcha", driver.findElement(WRONGMAILID).getText());
		// Assert.assertEquals("Your details are not recognised",
		// driver.findElement(WRONGMAILID).getText());

	}

	@When("^I enter registeration details$")
	public void i_enter_registeration_details() throws Throwable {
		driver.findElement(LOGINREG).click();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LOGINREG));
		driver.findElement(REGISTER).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(REGISTEREMAIL));
		driver.findElement(REGISTEREMAIL).clear();
		driver.findElement(REGISTEREMAIL).sendKeys("shruthi.a12@gmail.com");
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
		wait.until(ExpectedConditions.visibilityOfElementLocated(SELECTDROP));

		Select objSelect = new Select(driver.findElement(SELECTDROP));
		// objSelect.selectByIndex(3);
		objSelect.selectByValue("mrs");
		// objSelect.selectByVisibleText("Mrs");

		wait.until(ExpectedConditions.visibilityOfElementLocated(FIRSTNAME));
		driver.findElement(FIRSTNAME).clear();
		driver.findElement(FIRSTNAME).sendKeys("Shruthi");
		wait.until(ExpectedConditions.visibilityOfElementLocated(LASTNAME));
		driver.findElement(LASTNAME).clear();
		driver.findElement(LASTNAME).sendKeys("A");
		wait.until(ExpectedConditions.visibilityOfElementLocated(PASSWORD));
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc");
		wait.until(ExpectedConditions.visibilityOfElementLocated(CONFIRMPASSWORD));
		driver.findElement(CONFIRMPASSWORD).clear();
		driver.findElement(CONFIRMPASSWORD).sendKeys("def");

	}

	@Then("^I should get option to register$")
	public void i_should_get_option_to_register() throws Throwable {
		Assert.assertEquals("", driver.findElement(RECAPTCHA).getText());
		Assert.assertEquals("Register with Tu", driver.findElement(REGTU).getText());
	}

	@When("^I enter promotion id details$")
	public void i_enter_promotion_id_details() throws Throwable {
		driver.findElement(LOGINREG).click();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LOGINREG));
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
		driver.findElement(PROMOTIONS).clear();
		driver.findElement(PROMOTIONS).sendKeys("abc@gmail.com");
		driver.findElement(PROMOTIONSCLK).click();

	}

	@Then("^I should get details on promotion$")
	public void i_should_get_details_on_promotion() throws Throwable {

		// Assert.assertEquals("True", driver.findElement(PROMOTIONSTXT).

	}

	@Given("^I am in helpcentre page$")
	public void i_am_in_helpcentre_page() throws Throwable {
		driver.findElement(LOGINREG).click();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(LOGINREG));
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);

	}

	@When("^I click on help page$")
	public void i_click_on_help_page() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		driver.findElement(HELPCENTRE).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(HELPCENTRE));

	}

	@Then("^I should get Tu help information$")
	public void i_should_get_Tu_help_information() throws Throwable {
		Assert.assertEquals("Tu Help", driver.findElement(TUHELP).getText());
	}

	@When("^I click on contactus page$")
	public void i_click_on_contactus_page() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACTUS));
		driver.findElement(CONTACTUS).click();

	}

	@Then("^I should get contactus information$")
	public void i_should_get_contactus_information() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(CONTACTUS));
		Assert.assertEquals("Contact us", driver.findElement(CONTACTUS).getText());

	}

	@After
	public void close() {
		driver.close();

	}

}
