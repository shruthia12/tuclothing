Feature: Search

@regression @smoke @sprint-23 @TU-156
Scenario: Search with valid data

Given I am in home page
When I search valid data
Then I should related search results page



Scenario: Search with invalid data

Given I am in home page
When I search invalid data
Then I should related no results page


